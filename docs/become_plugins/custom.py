# -*- coding: utf-8 -*-
# Copyright: (c) 2021, Ansible Project
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = """
    name: custom
    short_description: Run tasks using custom command
    description:
        - This become plugins allows your remote/login user to execute commands as another user.
    options:
        become_user:
            description: User you 'become' to execute the task.
            default: root
            ini:
              - section: privilege_escalation
                key: become_user
              - section: sudo_become_plugin
                key: user
            vars:
              - name: ansible_become_user
              - name: ansible_sudo_user
            env:
              - name: ANSIBLE_BECOME_USER
              - name: ANSIBLE_SUDO_USER
        become_flags:
            ini:
              - section: privilege_escalation
                key: become_flags
              - section: sudo_become_plugin
                key: flags
            vars:
              - name: ansible_become_flags
              - name: ansible_sudo_flags
            env:
              - name: ANSIBLE_BECOME_FLAGS
              - name: ANSIBLE_SUDO_FLAGS
        become_pass:
            vars:
              - name: ansible_become_password
              - name: ansible_become_pass
              - name: ansible_sudo_pass
            env:
              - name: ANSIBLE_BECOME_PASS
              - name: ANSIBLE_SUDO_PASS
            ini:
              - section: sudo_become_plugin
                key: password
"""


from ansible.plugins.become import BecomeBase


class BecomeModule(BecomeBase):

    name = 'community.general.sudosu'

    # messages for detecting prompted password issues
    fail = ('Sorry, try again.',)
    missing = ('Sorry, a password is required to run sudo', 'sudo: a password is required')

    def build_become_command(self, cmd, shell):
        super(BecomeModule, self).build_become_command(cmd, shell)

        if not cmd:
            return cmd

        becomecmd = ''

        flags = self.get_option('become_flags') or ''
        prompt = ''
        if self.get_option('become_pass'):
            self.prompt = '[sudo via ansible, key=%s] password:' % self._id
            if flags:  # this could be simplified, but kept as is for now for backwards string matching
                flags = flags.replace('-n', '')
            prompt = '-p "%s"' % (self.prompt)

        user = self.get_option('become_user') or ''
        if user:
            user = '%s' % (user)

        return ' '.join([becomecmd, prompt,
            'checksum=$(md5sum /tmp/custom_become.sh | cut -c -32);',
            'if [ "$checksum" == "3b7f5cc2d368933e6e1ae5a511fda723" ];',
            ' then echo ""; else',
            'echo \'fakeTTY () { script -qefc "$(printf "%q " "$@")" /dev/null; }; TIMESTAMP=$(date +%s%N);TMP_DIR="/tmp/ansible_tmp_$TIMESTAMP";mkdir -p "$TMP_DIR";chmod 777 "$TMP_DIR";outputFile="$TMP_DIR"/output_custom_become;outputReady="$TMP_DIR"/output_ready_custom_become;fakeTTY sudo /usr/bin/rootsh -i -u "$1" >/dev/null 2>/dev/null << EOF\' > /tmp/custom_become.sh;',
            'echo \'export HISTFILE=/dev/null;(umask 000; touch "$outputFile");$2 > "$outputFile" 2> "$outputFile";(umask 000; touch "$outputReady");\' >> /tmp/custom_become.sh;',
            'echo \'EOF\' >> /tmp/custom_become.sh;',
            'echo \'until [ -f "$outputReady" ] ;do sleep 0.005; done ; cmd_output=$(< "$outputFile");rm -fR "$TMP_DIR";echo "$cmd_output";\' >> /tmp/custom_become.sh;',
            'chmod +x /tmp/custom_become.sh;',
            'fi;',
            '/tmp/custom_become.sh',
            user, "\""+self._build_success_command(cmd, shell)+"\""])
