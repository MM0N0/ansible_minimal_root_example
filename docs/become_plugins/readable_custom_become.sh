#------------------------
# fully readable Version
#------------------------

fakeTTY () {
   script -qefc "$(printf "%q " "$@")" /dev/null
}
TIMESTAMP=$(date +%s%N)
TMP_DIR="/tmp/ansible_tmp_$TIMESTAMP"
mkdir -p "$TMP_DIR"
chmod 777 "$TMP_DIR"
outputFile="$TMP_DIR"/output_custom_become
outputReady="$TMP_DIR"/output_ready_custom_become

# critical part here
fakeTTY sudo /usr/bin/rootsh -i -u "$1" >/dev/null 2>/dev/null << EOF
export HISTFILE=/dev/null;
(umask 000; touch "$outputFile");
$2 > "$outputFile" 2> "$outputFile";
(umask 000; touch "$outputReady");
EOF

until [ -f "$outputReady" ]
do
     sleep 0.005
done
cmd_output=$(< "$outputFile")
rm -fR "$TMP_DIR"
echo "$cmd_output"

#------------------------
# CompressedVersion
#------------------------
fakeTTY () { script -qefc "$(printf "%q " "$@")" /dev/null; }; TIMESTAMP=$(date +%s%N);TMP_DIR="/tmp/ansible_tmp_$TIMESTAMP";mkdir -p "$TMP_DIR";chmod 777 "$TMP_DIR";outputFile="$TMP_DIR"/output_custom_become;outputReady="$TMP_DIR"/output_ready_custom_become;fakeTTY sudo /usr/bin/rootsh -i -u "$1" >/dev/null 2>/dev/null << EOF
export HISTFILE=/dev/null;(umask 000; touch "$outputFile");$2 > "$outputFile" 2> "$outputFile";(umask 000; touch "$outputReady");
EOF
until [ -f "$outputReady" ] ;do sleep 0.005; done ; cmd_output=$(< "$outputFile");rm -fR "$TMP_DIR";echo "$cmd_output";

#------------------------
# Commands run by python script
# creating CompressedVersion via echo x >> file
# But also checking for md5 so its only written, when needed
#------------------------
checksum=$(md5sum /tmp/custom_become.sh | cut -c -32);
if [ "$checksum" == "3b7f5cc2d368933e6e1ae5a511fda723" ];
then echo ""; else
echo \'fakeTTY () { script -qefc "$(printf "%q " "$@")" /dev/null; }; TIMESTAMP=$(date +%s%N);TMP_DIR="/tmp/ansible_tmp_$TIMESTAMP";mkdir -p "$TMP_DIR";chmod 777 "$TMP_DIR";outputFile="$TMP_DIR"/output_custom_become;outputReady="$TMP_DIR"/output_ready_custom_become;fakeTTY sudo /usr/bin/rootsh -i -u "$1" >/dev/null 2>/dev/null << EOF\' > /tmp/custom_become.sh;
echo \'export HISTFILE=/dev/null;(umask 000; touch "$outputFile");$2 > "$outputFile" 2> "$outputFile";(umask 000; touch "$outputReady");\' >> /tmp/custom_become.sh;
echo \'EOF\' >> /tmp/custom_become.sh;
echo \'until [ -f "$outputReady" ] ;do sleep 0.005; done ; cmd_output=$(< "$outputFile");rm -fR "$TMP_DIR";
echo "$cmd_output";\' >> /tmp/custom_become.sh;
chmod +x /tmp/custom_become.sh;
fi;
/tmp/custom_become.sh
