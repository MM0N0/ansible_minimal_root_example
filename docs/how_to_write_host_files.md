# how to write host files
host files are yaml files in a directory structure, which ansible uses to evaluate variables in playbooks, tasks and roles.

the structure should be like 
[this](https://docs.ansible.com/ansible/2.8/user_guide/playbooks_best_practices.html#alternative-directory-layout).

here an example:
````
hosts/
├── all.yml
├── dev
│   ├── group_vars
│   │   ├── all.yml -> ../../all.yml
│   │   └── dev.yml
│   │   └── tomcat.yml  
│   ├── host_vars
│   │   ├── dev_tomcat.yml
│   │   └── dev_um.yml
│   └── hosts.yml
...
````

## global

global vars for the whole project should be defined in: 
<code>hosts/all.yml</code>
````
---
# vars defined here are valid
# for every host of every stage
ansible_ssh_extra_args: '-o ControlMaster=no -o ControlPersist=60s -o ConnectTimeout=10 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o GlobalKnownHostsFile=/dev/null'
template_dir: "{{ playbook_dir }}/templates/{{ stage_name }}/"

````

## stages

for the stage "dev" 
<code>hosts/dev/hosts.yml</code>
defines all hosts and assigns them to groups.
````
# declare hosts
proj_xyz:
  children:
    tomcat:
      hosts:
        dev_tomcat:
        dev_um:

# assign hosts to groups
    tomcat:
      hosts:
        dev_tomcat:
    um:
      hosts:
        dev_um:
````
### groups
inside <code>group_vars</code> you can place files with the name of the group.

the file <code>hosts/dev/group_vars/dev.yml</code> contains vars, which are set for all hosts assigned to the group. 
````
---
stage_name: "dev"
````
<code>dev</code> is a group as well, so 
in this example <code>stage_name</code> is set to "dev" 
for all hosts on the dev stage. 

### hosts

inside <code>host_vars</code> you can place files with the name of the host.

the file <code>hosts/dev/host_vars/dev_tomcat.yml</code> contains vars, which are set for the host <code>dev_tomcat</code>.
````
---
ansible_host: 8.8.8.8
````
<code>ansible_host</code> defines the ip ansible uses to connect to the host.
