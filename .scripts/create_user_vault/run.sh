#!/usr/bin/env bash
SCRIPT_DIR=${0%/*}
source "$SCRIPT_DIR/../.dev_env/project.conf"

docker run --rm -it --net="host" --name="create_user_vault_$PROJECT_NAME" \
  -v "${PWD}":/repo:rw \
  --mount type=tmpfs,destination=/ramdisk \
  "mm0n0/dev_env_$PROJECT_NAME:v1" \
  \
  bash -c "cd /repo && $SCRIPT_DIR/docker_script.sh"
