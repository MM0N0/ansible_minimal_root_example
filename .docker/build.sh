#!/usr/bin/env bash
SCRIPT_DIR=${0%/*}
source "$SCRIPT_DIR/../.dev_env/project.conf"

# build
# run build in "$SCRIPT_DIR/.." so .env is in the build context too
# and the repository can be copied to the docker image
docker build -f "$SCRIPT_DIR/Dockerfile_test_host" -t "${DOCKER_IMAGE_PREFIX}/test_host_$PROJECT_NAME:v1" "$SCRIPT_DIR"
