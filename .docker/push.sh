#!/usr/bin/env bash
SCRIPT_DIR=${0%/*}
source "$SCRIPT_DIR/../.dev_env/project.conf"

# publish
docker push "${DOCKER_IMAGE_PREFIX}/test_host_$PROJECT_NAME:v1"
