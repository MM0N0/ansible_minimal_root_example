[![test](https://gitlab.com/MM0N0/ansible_minimal_root_example/badges/main/pipeline.svg)](https://gitlab.com/MM0N0/ansible_minimal_root_example/-/pipelines)

# Ansible example Project - minimal_root
this in an example project of the ansible collection [minimal_root](https://gitlab.com/MM0N0/ansible_minimal_root).

**Note**: this is work in progress

## Requirements
- docker and docker-compose
- bash
- cat

note: if you are using windows, you will have to use wsl

## TL;DR - Quickstart
1. Install the requirements (see above)
2. generate your own 'user_vault.yml':
   ```bash
   .scripts/create_user_vault/run.sh
   ```
   
   **(in this example it is already generated with vault_pass: 'password', so you can skip this step)**

3. install required ansible collections:

   ```bash
   .scripts/install_ansible_requirements.sh
   ```
4. enter dev_env:
   ```bash
   ./dev_env.sh bash
   ```
   It will ask you to type your 'user_vault.yml' password you used in step #1.
   This vault password will be applied to all ansible commands you run inside the container. 

After the setup I recommend setting up the local stage to see if everything works.

## How to use
### how to run the master playbook?
```bash
./dev_env.sh ansible-playbook -i hosts/<STAGE> -i user_vault.yml playbooks/master.yml
```

### production changes
To run the playbook on production, you will have to approve while calling the playbook (see [check_stage.yml](playbooks/master/check_stage.yml)).

In a non-interactive environment you will have to add an extra variable to the command:
```bash
./dev_env.sh ansible-playbook -i hosts/prod -i user_vault.yml playbooks/master.yml -e "allow_prod_change=1" 
```

## local stage
the local stage is meant to be a stage identical with the dev stage, but being entirely local as docker containers.

1. start local docker containers with:
   ```bash
   .docker/local_setup/start.sh
   ```
2. run master playbook for stage 'local'
   ```bash
   ./dev_env.sh ansible-playbook -i hosts/local -i user_vault.yml playbooks/master.yml
   ```
3. stop local docker containers with:
   ```bash
   .docker/local_setup/stop.sh
   ```

## Documentation
### user_vault.yml
The inventory file 'user_vault.yml' contains all your personal credentials. These values **must** be encrypted!
I recommend encrypting single values you want to protect. (see: [encrypting-individual-variables-with-ansible-vault](https://docs.ansible.com/ansible/latest/vault_guide/vault_encrypting_content.html#encrypting-individual-variables-with-ansible-vault))


For a quick start, you can use the following command to create your user_vault.yml interactively:
```bash
./dev_env.sh .docker/create_user_vault.sh
```
If you want to do it manually copy [user_vault.yml.template](user_vault.yml.template) to user_vault.yml and adjust to your needs (change the env vars for encrypted vaules).

note: **user_vault.yml must not be a symlink!**

In a standard project you don't want to check 'user_vault.yml' into the repository.

### dev_env

the dev_env is meant to be a development environment and all ansible commands should be run inside of it.

You can enter it and run the commands in the new shell:
```bash
./dev_env.sh bash
# then run ansible commands:
[ansible_minimal_root_example] ansible-playbook -i hosts/local -i user_vault.yml playbooks/master.yml
```

Or you can run the commands inline:
```bash
./dev_env.sh ansible-playbook -i hosts/local -i user_vault.yml playbooks/master.yml
```

#### SSH KEY
If you need a key to connect to the hosts, you can inject an ssh key to into the environment. set the path to the ssh keyfile with:
```bash
export SSH_KEY_FILE=<PATH_TO_SSH_KEYFILE>
```
or just set it for a single session with:
```bash
SSH_KEY_FILE=<PATH_TO_SSH_KEYFILE> ./dev_env.sh bash
```
You will need to type your passphrase when you enter the container.

#### VAULT PASSWORDS
the docker env will inject ansible vault passwords into the environment, if you set the path to the vault password file for the vault 'user_vault' with:
```bash
export ANSIBLE_VAULT_PASS_FILE_user_vault=<PATH_TO_VAULT_PASSWORD_FILE>
```
if you don't set this variable, you will have to enter the vault password manually, when entering the docker environment.

You can configure the vault ids for which this behaviour applies in [.dev_env/.ansible_vault_ids](.dev_env/.ansible_vault_ids).

**there will be no prompting for vault ids not configured in this file**


### script - create_user_vault
The user_vault.yml will be generated based on "[user_vault.yml.template](user_vault.yml.template)".
This script will prompt the user for all Env vars found inside 'user_vault.yml.template'.
So you don't need to edit this file, just update "user_vault.yml.template".

## use this project as a template
1. copy all files to a new repository (make sure the symlinks are still there and to add permissions to all scripts) these scripts may be helpful:
   1. permissions:         'find . -name "*.sh" -exec chmod +x {} \;'
   2. git permissions:    'find . -name "*.sh" -exec git update-index --chmod=+x {} \;'
2. remove '[user_vault.yml](user_vault.yml)' and '[vault_pass_file](vault_pass_file)' from the repository and add it to .gitignore
3. Edit README.md. Remove snippets from:
   - TL;DR - Quickstart/#2 (last line)
   - [## use this project as a template]
4. update hosts (see [how_to_write_host_files.md](docs/how_to_write_host_files.md))
5. update local stage
   1. add new hosts to [docker-compose.yml](.docker/local_setup/docker-compose.yml) and set different ports for each
   2. add new file for each host in [host_vars](hosts/dev/host_vars) with matching ports
   3. symlink all new groups in [group_vars](hosts/dev/group_vars) to link to the dev stage
   4. add new hosts to [hosts.yml](hosts/local/hosts.yml)
6. write master playbooks (look at the examples)
7. update [.dev_env/.ansible_vault_ids](.dev_env/.ansible_vault_ids)
8. update [user_vault.yml.template](user_vault.yml.template)

## TODOs
- ...

## Contribute
...

## License

GNU General Public License v3.0 or later

See [COPYING](COPYING) to see the full text.
