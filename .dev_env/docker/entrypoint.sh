#!/usr/bin/env bash
function cache_vault_pass() {
    VAULT_ID="$1"
    VAULT_PASS_FILE="/ramdisk/$VAULT_ID"

    VAULT_PASS_ENV_VAR_NAME="ANSIBLE_VAULT_PASS_$VAULT_ID"

    if [[ -n ${!VAULT_PASS_ENV_VAR_NAME} && $VAULT_PASS_ENV_VAR_NAME != "" ]]; then
      declare -n VAULT_PASS_ENV_VAR_NAME="$VAULT_PASS_ENV_VAR_NAME"
      echo "${PS1}info: found pass for vault with id '$VAULT_ID'";
      VAULT_PASS=${VAULT_PASS_ENV_VAR_NAME}
    else
      read -r -s -p "[$VAULT_ID] type vault password: " VAULT_PASS
      echo ""
    fi
    if [[ -n $VAULT_PASS && $VAULT_PASS != "" ]]; then
      echo "$VAULT_PASS" > "$VAULT_PASS_FILE"
      ANSIBLE_VAULT_IDENTITY_LIST="$ANSIBLE_VAULT_IDENTITY_LIST,$VAULT_PASS_FILE"
    fi
}
function inject_key_into_docker() {
  SSH_KEY_CONTENT=$1

  mkdir /root/.ssh;
  chmod 700 /root/.ssh;
  echo "$SSH_KEY_CONTENT" | tr "\t" "\n" > /root/.ssh/id_rsa
  printf 'IdentityFile /root/.ssh/id_rsa' > /root/.ssh/config;
  cat "$HOME/.ssh/known_hosts" > /root/.ssh/known_hosts;
  chmod 600 /root/.ssh/*;
  # shellcheck disable=SC2006
  # shellcheck disable=SC2046
  eval `ssh-agent`;
  ssh-add /root/.ssh/id_rsa;
}

SCRIPT_DIR=${0%/*}
source "${SCRIPT_DIR}/../project.conf"

# set custom prompt
export PS1="[$PROJECT_NAME] "

# injecting SSH KEY

if [ "$SSH_KEY_CONTENT" != "" ]; then
  echo "${PS1}info: injecting ssh key"
  inject_key_into_docker "$SSH_KEY_CONTENT"
# else
#   echo "${PS1}info: no ssh key file provided"
fi

# run cache_vault_pass for vault passwords
VAULT_IDS=$(cat "${SCRIPT_DIR}/../.ansible_vault_ids")
for vault_id in $VAULT_IDS
do
  eval "cache_vault_pass '$vault_id' \"\$ANSIBLE_VAULT_PASS_$vault_id\";"
done

export ANSIBLE_VAULT_IDENTITY_LIST

# run commands inside dev_env
bash -c "$1 $2 $3 $4 $5 $6 $7 $8 $9 ${10} ${11} ${12} ${13} ${14} ${15} ${16} ${17} ${18}"
