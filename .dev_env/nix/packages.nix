let
    fixed_pkgs = import ./pinned.nix;
in
[
    # add project specific packages here
    
    fixed_pkgs.sshpass
    fixed_pkgs.ansible_2_13
    
]
